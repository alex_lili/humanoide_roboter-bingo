<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Bingo" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="Bingo_Winner_Detection" src="Bingo_Winner_Detection/Bingo_Winner_Detection.dlg" />
    </Dialogs>
    <Resources>
        <File name="" src=".metadata" />
        <File name="xalinfo" src="xalinfo" />
        <File name="bingo_main" src="html/bingo_main.html" />
        <File name="bootstrap.min" src="html/js_css/bootstrap.min.css" />
        <File name="bootstrap.min" src="html/js_css/bootstrap.min.js" />
        <File name="jquery-3.3.1.min" src="html/js_css/jquery-3.3.1.min.js" />
        <File name="jquery.min" src="html/js_css/jquery.min.js" />
        <File name="popper.min" src="html/js_css/popper.min.js" />
        <File name="robotutils" src="html/js_css/robotutils.js" />
        <File name="touch" src="html/js_css/touch.js" />
        <File name="w3" src="html/js_css/w3.css" />
        <File name="bingo_end_converted" src="html/resources/bingo_end_converted.mp4" />
        <File name="bingo_rules_converted" src="html/resources/bingo_rules_converted.mp4" />
        <File name="bingo_fireworks" src="html/resources/bingo_fireworks.gif" />
        <File name="bingo_applause" src="html/resources/bingo_applause.wav" />
        <File name="volume_down" src="html/resources/volume_down.png" />
        <File name="volume_up" src="html/resources/volume_up.png" />
    </Resources>
    <Topics>
        <Topic name="Bingo_Winner_Detection_ged" src="Bingo_Winner_Detection/Bingo_Winner_Detection_ged.top" topicName="Bingo_Winner_Detection" language="de_DE" />
    </Topics>
    <IgnoredPaths />
    <Translations auto-fill="de_DE">
        <Translation name="translation_de_DE" src="translations/translation_de_DE.ts" language="de_DE" />
    </Translations>
</Package>
