<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Intro/Say</name>
        <message>
            <source>Hallo</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Hallo, wir wollen heute Bingo spielen.</source>
            <comment>Text</comment>
            <translation type="unfinished">Hallo, wir wollen heute Bingo spielen.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Intro/Say (1)</name>
        <message>
            <source>Hallo</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
        <message>
            <source>Wählen Sie eine Bingo Art.</source>
            <comment>Text</comment>
            <translation type="obsolete">Wählen Sie eine Bingo Art.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Intro/Say (2)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Los gehts.</source>
            <comment>Text</comment>
            <translation type="unfinished">Los gehts.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Intro/Say Global Bodytalk (1)/Say Global</name>
        <message>
            <location filename="C:/WINDOWS/System32/translations/behavior_1/behavior.xar" line="0"/>
            <source></source>
            <comment>Text</comment>
            <translation></translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Intro/Say Global Bodytalk/Say Global</name>
        <message>
            <location filename="C:/WINDOWS/System32/translations/behavior_1/behavior.xar" line="0"/>
            <source></source>
            <comment>Text</comment>
            <translation></translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Number Generator 50/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Outro/Say Outro</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Spiel vorbei!</source>
            <comment>Text</comment>
            <translation type="unfinished">Spiel vorbei!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Regeln/Say</name>
        <message>
            <source>Hallo</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
        <message>
            <source>Hier kommen die Regeln.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hier kommen die Regeln.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Regeln/Say Global Bodytalk/Say Global</name>
        <message>
            <location filename="C:/WINDOWS/System32/translations/behavior_1/behavior.xar" line="0"/>
            <source></source>
            <comment>Text</comment>
            <translation></translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Rules/Say</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Hier kommen die Regeln.</source>
            <comment>Text</comment>
            <translation type="unfinished">Hier kommen die Regeln.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hallo</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
        <message>
            <source>Bingo-Typ konnte nicht erkannt werden!</source>
            <comment>Text</comment>
            <translation type="obsolete">Bingo-Typ konnte nicht erkannt werden!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (1)</name>
        <message>
            <source>Hallo</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
        <message>
            <source>Spiel vorbei!</source>
            <comment>Text</comment>
            <translation type="obsolete">Spiel vorbei!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (2)</name>
        <message>
            <source>Hallo</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
        <message>
            <source>Los gehts.</source>
            <comment>Text</comment>
            <translation type="obsolete">Los gehts.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (4)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (5)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hallo</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say Global Bodytalk/Say Global</name>
        <message>
            <location filename="C:/WINDOWS/System32/translations/behavior_1/behavior.xar" line="0"/>
            <source></source>
            <comment>Text</comment>
            <translation></translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say Outro</name>
        <message>
            <source>Spiel vorbei!</source>
            <comment>Text</comment>
            <translation type="obsolete">Spiel vorbei!</translation>
        </message>
    </context>
</TS>
