var locked = false;
var memory;

// function to catch missing console on robot
// only usable if used on local browser
function logthis(message){
	try{
		console.log(message);
	} catch(err){}
}

// Callback touch event
function onTouchDown(data){
	logthis("touch down");
	if( !locked ){
	
		// recalculate data
		x = data[0] * $(document).width();
		y = data[1] * $(document).height();
	
		// get element
		locked = true;
		var el = document.elementFromPoint(x, y);
		
		if( el != null ){
			// create and sipatch event
			var ev = new MouseEvent('click', {
				'view': window,
				'bubbles': true,
				'cancelable': false
			});
			el.dispatchEvent(ev);
			
			// check if element or parent element is button
			if( el.tagName == 'BUTTON' ){
				logthis(el);
				memory.raiseEvent( "custom/tablet/onButtonClick", el.id );
			} else if( el.parentElement != null && el.parentElement.tagName == 'BUTTON' ){
				logthis(el.parentElement);
				memory.raiseEvent( "custom/tablet/onButtonClick", el.parentElement.id );
			}
		}
		
	}
}

function alOnClick(value){
	memory.raiseEvent( "custom/tablet/onButtonClick", value );
}

function onTouchUp(){
	locked = false;
}

//Bingo_Data is loaded into website elements
function loadBingoData(bingo_data_string){
	
	var bingo_data = JSON.parse(bingo_data_string);
	
	var number_array = bingo_data['numbers'];
	var bingo_type = bingo_data['bingo_type'];
	
	var current_rand_number = 0;
	var previous_number = 0;
	
	if(number_array.length > 1)
	{
		previous_number = number_array[number_array.length - 2];
		document.getElementById("previous_number").innerHTML = previous_number;
	}
 
	current_rand_number = number_array[number_array.length - 1];
	
	document.getElementById("current_rand_number").innerHTML = current_rand_number;
	
	document.getElementById("navbar_title").innerHTML += bingo_type + "&emsp;&emsp;Zahl Nr. " + number_array.length;
	
	//Modal wird mit Zahlen befüllt
	var area_multiplier = 0;
	var B_range_string = "";
	var I_range_string = "";
	var N_range_string = "";
	var G_range_string = "";
	var O_range_string = "";
	
	if(bingo_type == 40 || bingo_type == 50)
	{
		area_multiplier = 2;
	}
	else if(bingo_type == 75)
	{
		area_multiplier = 3;
	}
	
	for (i in number_array){
		if(number_array[i]>=1 && number_array[i] <= (area_multiplier*5) )
		{
			B_range_string += ("&nbsp;&nbsp;" + number_array[i]);
		}
		else if(number_array[i]>= (area_multiplier*5+1) && number_array[i]<= (area_multiplier*10))
		{
			I_range_string += ("&nbsp;&nbsp;" + number_array[i]);
		}
		else if(number_array[i]>= (area_multiplier*10+1) && number_array[i]<= (area_multiplier*15))
		{
			N_range_string += ("&nbsp;&nbsp;" + number_array[i]);
		}
		else if(number_array[i]>= (area_multiplier*15+1) && number_array[i]<= (area_multiplier*20))
		{
			G_range_string += ("&nbsp;&nbsp;" + number_array[i]);
		}
		else if(number_array[i]>= (area_multiplier*20+1) && number_array[i]<= (area_multiplier*25))
		{
			O_range_string += ("&nbsp;&nbsp;" + number_array[i]);
		}
	}
	
	document.getElementById("B_range").innerHTML += B_range_string;
	document.getElementById("I_range").innerHTML += I_range_string;
	document.getElementById("N_range").innerHTML += N_range_string;
	document.getElementById("G_range").innerHTML += G_range_string;
	document.getElementById("O_range").innerHTML += O_range_string;
	
	if(bingo_type == 40)
	{
		document.getElementById("O_range_title").innerHTML = "";
		document.getElementById("O_range").innerHTML = "";
	}
}


// NAOqi connected
function onConnected(session){
	logthis("connected to naoqi");
	session.service("ALMemory").then( 
		function (service){
			memory = service;
			RobotUtils.subscribeToALMemoryEvent( "custom/tablet/onTouchDown", onTouchDown );
			RobotUtils.subscribeToALMemoryEvent( "custom/tablet/onTouchUp", onTouchUp );
			RobotUtils.subscribeToALMemoryEvent( "custom/BingoData", loadBingoData );
			
			// set website alive interval
			setInterval( function(){
				memory.raiseEvent( "custom/tablet/alive", 1 );
			}, 1000 );
		},
		function (error) {}
	);
}

// NAOqi disconnected
function onDisconnected(){
	alert("Disconnected from NAOqi!");
}

// ------ MAIN ------
$( document ).ready(function() {
	logthis("document ready");

    // connect to naoqi
    // function will wait half a second after document is ready,
    // because connected sometimes fails if done without timeout.
    setTimeout( function(){
    	logthis("connecting to naoqi...");
		RobotUtils.connect( onConnected, onDisconnected );
	}, 500 );
});



